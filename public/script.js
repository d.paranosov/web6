
const type1 = 90;
const type2 = 75;
const type3 = 75;
const zero = 0;

function radioFunc() {
var myradios = document.getElementById("myradios");
myradios.innerHTML = "Добавки:";
var radio1 = document.createElement("input");
radio1.type = "radio";
radio1.checked = "checked";
radio1.value = 20;
radio1.name = "myradio";
var label = document.createElement("label");
label.appendChild(radio1);
myradios.appendChild(label);
label.innerHTML += "С фундуком";

var radio2 = document.createElement("input");
radio2.type = "radio";
radio2.value = 25;
radio2.name = "myradio";
var label = document.createElement("label");
label.appendChild(radio2);
myradios.appendChild(label);
label.innerHTML += "С варёной сгущёнкой";

var radio3 = document.createElement("input");
radio3.type = "radio";
radio3.value = 23;
radio3.name = "myradio";
var label = document.createElement("label");
label.appendChild(radio3);
myradios.appendChild(label);
label.innerHTML += "С изюмом";
}

function checkboxFunc() {
var mycheckbox = document.getElementById("mycheckbox");
mycheckbox.innerHTML = "Добавки:";
var checkbox1 = document.createElement("input");
checkbox1.type = "checkbox";
checkbox1.id = "idCheckbox1"
checkbox1.value = 20;
checkbox1.name = "check-1";
var label = document.createElement("label");
label.appendChild(checkbox1);
mycheckbox.appendChild(label);
label.innerHTML += "Фундук";

var checkbox2 = document.createElement("input");
checkbox2.type = "checkbox";
checkbox2.id = "idCheckbox2"
checkbox2.value = 25;
checkbox2.name = "check-2";
var label = document.createElement("label");
label.appendChild(checkbox2);
mycheckbox.appendChild(label);
label.innerHTML += "Варёная сгущёнка";

var checkbox3 = document.createElement("input");
checkbox3.type = "checkbox";
checkbox3.id = "idCheckbox3"
checkbox3.value = 23;
checkbox3.name = "check-3";
var label = document.createElement("label");
label.appendChild(checkbox3);
mycheckbox.appendChild(label);
label.innerHTML += "Изюм";
}


  window.addEventListener('DOMContentLoaded', function (event) {
    let s = document.getElementsByName("myselect");
    var select;
    var quantity = 0;
    s[0].addEventListener("change", function(event) {
      select = event.target;
      let radios = document.getElementById("myradios");
      let checkbox = document.getElementById("mycheckbox");
      console.log(select.value);
      console.log(quantity.value);
      if(select.value == "0") {
        radios.style.display = "none";
        checkbox.style.display = "none";
        res.innerHTML="";
      }
      if (select.value == "1") {
        radios.style.display = "none";
        checkbox.style.display = "none";
        res.innerHTML="Стоимость заказа: " + type1*
        (typeof quantity.value === 'undefined'? zero : quantity.value);
      }
      if (select.value == "2") {
        radios.style.display = "block";
        checkbox.style.display = "none";
        radioFunc();
        res.innerHTML="Стоимость заказа: " + (type2 + b)*
        (typeof quantity.value === 'undefined'? zero : quantity.value);
      }
      if(select.value == "3") { 
        radios.style.display = "none";
        checkbox.style.display = "block";
        checkboxFunc();
        res.innerHTML="Стоимость заказа: " + (type3 + sum)*
        (typeof quantity.value === 'undefined'? zero : quantity.value);
      }
    });
    
    var res = document.getElementById("result");

    let q = document.querySelectorAll(".myfield");
    q.forEach(function(number) {
      number.addEventListener("change", function(event) {
        quantity = event.target;
        console.log(parseInt(quantity.value));
        if(quantity.value<0){
          alert("Ошибка: число не должно быть отрицательным");
          return false;
        }
        var m = quantity.value.match(/\D/g);
        if (m !== null) {
          alert("Ошибка: некорректный ввод данных, введите число");
          return false;
        }
        
        if(select.value === "1") {
          res.innerHTML="Стоимость заказа: " + type1*quantity.value;
        }
        if(select.value === "2") {
          res.innerHTML="Стоимость заказа: " + (type2 + b)*quantity.value;
        } 
        if(select.value === "3") {
          res.innerHTML="Стоимость заказа: " + (type3 + sum)*quantity.value;
        }
      });

    });

    let r = document.querySelectorAll(".myradios");
    var ra; var b = 0;
    r.forEach(function(radio) {
      radio.addEventListener("change", function(event) {
        ra = event.target;
        b=parseInt(ra.value);
        console.log(b*quantity.value);
        res.innerHTML="Стоимость заказа: " + (typeof quantity.value === 'undefined'?
        zero : (type2 + b)*quantity.value);
        });    
    });

    let c = document.querySelectorAll(".mycheckbox");
    var ch; var sum = 0;
    c.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
          ch = event.target;
          if(idCheckbox1.checked) {
            idCheckbox1.value = 20;
          }
          else {
            idCheckbox1.value = 0;
          }
          if(idCheckbox2.checked) {
            idCheckbox2.value = 25;
          }
          else {
            idCheckbox2.value = 0;
          }
          if(idCheckbox3.checked) {
            idCheckbox3.value = 23;
          }
          else {
            idCheckbox3.value = 0;
          }
          sum = parseInt(idCheckbox1.value) + parseInt(idCheckbox2.value) +
          parseInt(idCheckbox3.value);
            console.log(ch.value*quantity.value);
            res.innerHTML="Стоимость заказа: " + (typeof quantity.value === 'undefined'?
            zero : (type3 + sum)*quantity.value);
        });
    });
});

  document.body.style.background = "#D8BFD8";
